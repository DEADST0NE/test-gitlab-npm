"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var RabbitmqService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RabbitmqService = exports.RabbitReadyState = void 0;
const common_1 = require("@nestjs/common");
const amqplib_1 = require("amqplib");
const constants_1 = require("./constants");
var RabbitReadyState;
(function (RabbitReadyState) {
    RabbitReadyState[RabbitReadyState["closed"] = 0] = "closed";
    RabbitReadyState[RabbitReadyState["opened"] = 1] = "opened";
    RabbitReadyState[RabbitReadyState["connecting"] = 2] = "connecting";
})(RabbitReadyState || (exports.RabbitReadyState = RabbitReadyState = {}));
let RabbitmqService = RabbitmqService_1 = class RabbitmqService {
    constructor(options) {
        this.options = options;
        this.logger = new common_1.Logger(RabbitmqService_1.name);
        this.connected = false;
        this.stayAlive = false;
        this.RECONNECT_TTL = 3000;
        this.state = RabbitReadyState.closed;
    }
    async onModuleInit() {
        await this.connect();
    }
    async onModuleDestroy() {
        await this.disconnect();
    }
    async connect() {
        this.stayAlive = true;
        if (this.connected || this.state === RabbitReadyState.connecting) {
            return;
        }
        this.state = RabbitReadyState.connecting;
        try {
            this.connection = await (0, amqplib_1.connect)(this.options.url, {
                clientProperties: { connection_name: this.options.connectionName },
            });
        }
        catch (err) {
            this.logger.error(`Rabbit mq connection failed`);
            this.logger.log(err);
            this.state = RabbitReadyState.closed;
            this.connected = false;
            await this.reconnect();
            return;
        }
        this.connected = true;
        this.connection.on('close', () => {
            this.connected = false;
            this.state = RabbitReadyState.closed;
            if (this.stayAlive) {
                void this.reconnect();
            }
        });
        this.connection.on('error', (error) => {
            this.logger.error(`AMQP error `, error);
        });
        this.logger.debug(`Rabbit connected`);
        this.channel = await this.connection.createChannel();
        this.state = RabbitReadyState.opened;
        this.logger.debug(`Rabbit channel created`);
    }
    async reconnect() {
        await new Promise((r) => setTimeout(() => r(true), this.RECONNECT_TTL));
        await this.connect();
    }
    async disconnect() {
        this.stayAlive = false;
        this.connection?.removeAllListeners();
        await this.channel?.close();
        this.logger.debug(`Rabbit channel closed`);
        await this.connection?.close();
        this.logger.debug(`Rabbit disconnected`);
        this.channel = undefined;
        this.connection = undefined;
        this.connected = false;
        this.state = RabbitReadyState.closed;
    }
    async assetExchange(name, options) {
        this.logger.debug(`Asset exchange ${name}`);
        await this.connect();
        return await this.channel.assertExchange(name, options.type, options);
    }
    async assertQueue(name, options) {
        this.logger.debug(`Assert queue ${name}`);
        await this.connect();
        const queue = await this.channel.assertQueue(name, options);
        if (options.prefetchCount) {
            await this.channel.prefetch(options.prefetchCount);
        }
        if (options.exchange?.length) {
            await this.channel.bindQueue(queue.queue, options.exchange, options.pattern);
        }
        return queue.queue;
    }
    async sendToQueue(queue, data, options) {
        const payload = JSON.stringify(data);
        this.channel.sendToQueue(queue, Buffer.from(payload, 'utf-8'), options);
    }
    async subscribeToQueue(queue, callback, options = {}) {
        const consumer = await this.channel.consume(queue, async (msg) => {
            const content = msg.content.toString('utf-8');
            let payload;
            try {
                payload = JSON.parse(content);
            }
            catch (error) {
                this.logger.debug(`Invalid format message from queue`, content);
                this.channel.ack(msg);
                return;
            }
            await callback(payload, (ack) => {
                if (ack) {
                    this.channel.ack(msg);
                }
                else {
                    this.channel.nack(msg);
                }
            }, msg);
        }, {
            noAck: options.noAck,
        });
        return consumer.consumerTag;
    }
};
exports.RabbitmqService = RabbitmqService;
exports.RabbitmqService = RabbitmqService = RabbitmqService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(constants_1.RABBIT_MODULE_OPTIONS)),
    __metadata("design:paramtypes", [Object])
], RabbitmqService);
//# sourceMappingURL=rabbitmq.service.js.map