import { DynamicModule } from '@nestjs/common';
import { RabbitModuleAsyncOptions, RabbitModuleOptions } from './types';
export declare class RabbitmqModule {
    static forRoot(options: RabbitModuleOptions): DynamicModule;
    static forRootAsync(options: RabbitModuleAsyncOptions): DynamicModule;
}
