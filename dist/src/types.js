"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExchangeType = void 0;
var ExchangeType;
(function (ExchangeType) {
    ExchangeType["direct"] = "direct";
    ExchangeType["topic"] = "topic";
    ExchangeType["headers"] = "headers";
    ExchangeType["fanout"] = "fanout";
    ExchangeType["match"] = "match";
})(ExchangeType || (exports.ExchangeType = ExchangeType = {}));
//# sourceMappingURL=types.js.map