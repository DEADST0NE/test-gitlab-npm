"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var RabbitmqModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RabbitmqModule = void 0;
const common_1 = require("@nestjs/common");
const rabbitmq_service_1 = require("./rabbitmq.service");
const constants_1 = require("./constants");
let RabbitmqModule = RabbitmqModule_1 = class RabbitmqModule {
    static forRoot(options) {
        return {
            module: RabbitmqModule_1,
            providers: [
                {
                    provide: constants_1.RABBIT_MODULE_OPTIONS,
                    useValue: options,
                },
                rabbitmq_service_1.RabbitmqService,
            ],
            exports: [rabbitmq_service_1.RabbitmqService],
        };
    }
    static forRootAsync(options) {
        return {
            module: RabbitmqModule_1,
            imports: options.imports || [],
            providers: [
                {
                    provide: constants_1.RABBIT_MODULE_OPTIONS,
                    useFactory: options.useFactory,
                    inject: options.inject || [],
                },
                rabbitmq_service_1.RabbitmqService,
            ],
            exports: [rabbitmq_service_1.RabbitmqService],
        };
    }
};
exports.RabbitmqModule = RabbitmqModule;
exports.RabbitmqModule = RabbitmqModule = RabbitmqModule_1 = __decorate([
    (0, common_1.Module)({
        providers: [rabbitmq_service_1.RabbitmqService],
        exports: [rabbitmq_service_1.RabbitmqService],
    })
], RabbitmqModule);
//# sourceMappingURL=rabbitmq.module.js.map