import { OnModuleInit, OnModuleDestroy } from '@nestjs/common';
import { QueueOptions, QueueCallback, MessageOptions, ExchangeOptions, RabbitModuleOptions } from './types';
export declare enum RabbitReadyState {
    closed = 0,
    opened = 1,
    connecting = 2
}
export declare class RabbitmqService implements OnModuleInit, OnModuleDestroy {
    private readonly options;
    constructor(options: RabbitModuleOptions);
    private readonly logger;
    connected: boolean;
    private stayAlive;
    private channel?;
    private RECONNECT_TTL;
    private connection?;
    private state;
    onModuleInit(): Promise<void>;
    onModuleDestroy(): Promise<void>;
    connect(): Promise<void>;
    reconnect(): Promise<void>;
    disconnect(): Promise<void>;
    assetExchange(name: string, options: ExchangeOptions): Promise<import("amqplib").Replies.AssertExchange>;
    assertQueue(name: string, options: QueueOptions): Promise<string>;
    sendToQueue(queue: string, data: {
        [key: string]: any;
    }, options: MessageOptions): Promise<void>;
    subscribeToQueue(queue: string, callback: QueueCallback, options?: QueueOptions): Promise<string>;
}
